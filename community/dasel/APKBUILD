# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=1.21.2
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export GOPATH="$srcdir"
export GOFLAGS="$GOFLAGS -trimpath -modcacherw"
export CGO_ENABLED=0

build() {
	go build \
		-ldflags "-X github.com/tomwright/dasel/internal.Version=$pkgver" \
		-v \
		./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel "$pkgdir"/usr/bin/dasel
}

sha512sums="
0ba440ef28b8a4c9c0356a234154669a663cc217c073376afabea9cebee944b5b431530f2bd3fa9ad6e1a928ae67016d773716aa93e4b407f7fbf35138694fc2  dasel-1.21.2.tar.gz
"
