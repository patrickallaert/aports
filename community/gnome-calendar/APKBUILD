# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-calendar
pkgver=41.0
pkgrel=0
pkgdesc="Calendar application for GNOME"
url="https://wiki.gnome.org/Apps/Calendar"
# s390x, mips64 and riscv64 blocked by rust -> libhandy
arch="all !s390x !mips64 !riscv64"
license="GPL-3.0-or-later"
depends="gsettings-desktop-schemas"
makedepends="meson libical-dev gsettings-desktop-schemas-dev evolution-data-server-dev
	libsoup-dev libdazzle-dev glib-dev gtk+3.0-dev libgweather-dev geoclue-dev
	geocode-glib-dev gtk-doc libhandy1-dev"
options="!check" # Can't be run on release builds and we don't want debug asserts
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/gnome-calendar/${pkgver%.*}/gnome-calendar-$pkgver.tar.xz"

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
ba57af23f967a0900fd64399d9358c2dd131be86120e01d68cd4eee436e8a0bac6f4a3b154ffcfbb2d307ae632cfed7de29f79f843e4692cc1a81a82dadc5e4a  gnome-calendar-41.0.tar.xz
"
