# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname="icingaweb2-module-incubator"
_module=${pkgname/*-/}
pkgver=0.8.0
pkgrel=0
pkgdesc="Bleeding edge libraries useful for Icinga Web 2 modules"
url="https://www.icinga.org"
arch="noarch !armhf !armv7 !s390x !mips !mips64 !x86"
license="MIT"
options="!check"
_php=php8
depends="icingaweb2"
pkggroups="icingaweb2"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Icinga/$pkgname/archive/v$pkgver.tar.gz"

build() {
	return 0
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir/etc/icingaweb2/modules/$_module"
	mkdir -p "$pkgdir/usr/share/doc/$pkgname"
	mkdir -p "$pkgdir/usr/share/webapps/icingaweb2/modules/$_module"
	cp -a vendor composer.json composer.lock module.info run.php \
		"$pkgdir/usr/share/webapps/icingaweb2/modules/$_module"
	chgrp -R $pkggroups "$pkgdir/etc/icingaweb2/modules/$_module"

	cat >"$pkgdir"/usr/share/doc/$pkgname/README.alpine <<EOF
You need to fix /etc/icingaweb2/modules/$_module with the owner of the user of your webserver

For nginx, as example:
    # chown -R nginx /etc/icingaweb2/modules/$_module
or
    # chown -R nobody /etc/icingaweb2/modules/$_module

For Apache:
    # chown -R apache /etc/icingaweb2/modules/$_module

For lighttpd:
    # chown -R lighttpd /etc/icingaweb2/modules/$_module

Remember to enable the module with:

    # icingacli module enable $_module

EOF

}


sha512sums="
9f9ba7b86743ac3431e7b65e6d8984b08ec96de0dd83b3a022b6943d85c59bc696f87cf75533e635fe0f2b67e3b5f465320bf5ce483dc9c5b7182007fc1b3f12  icingaweb2-module-incubator-0.8.0.tar.gz
"
